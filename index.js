const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express();

mongoose.connect('mongodb+srv://Yurii:iPnbaYqbRpdXPNgC@yuriikovalchukstudy.hfiyktc.mongodb.net/?retryWrites=true&w=majority');

const { authRouter } = require('./Auth/authRouter');
const { notesRouter } = require('./Notes/notesRouter');
const { usersRouter } = require('./Users/usersRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/users', usersRouter);
app.use('/api/notes', notesRouter);
app.use('/api/auth', authRouter);

const start = async () => {
  app.listen(8080, () => console.log('Server started listening on pc'));
};

start();

// ERROR HANDLER
function errorHandler(err, req, res) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}

app.use(errorHandler);
