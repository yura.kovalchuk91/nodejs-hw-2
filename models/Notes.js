const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
  text: {
    type: String,
    required: true,
    min: 1,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  completed: {
    type: Boolean,
    required: true,
    default: false,
  },
}, { timestamps: true });

const Note = mongoose.model('Notes', noteSchema);

module.exports = {
  Note,
};
