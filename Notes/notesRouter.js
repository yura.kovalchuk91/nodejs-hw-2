const express = require('express');

const router = express.Router();
const {
  createNote, getNotes, getNote, deleteNote, markMyNoteCompletedById, updateNoteById,
} = require('./notesService');
const { authMiddleware } = require('../middleware/authMiddleware');

router.post('/', authMiddleware, createNote);
router.get('/', authMiddleware, getNotes);
router.get('/:id', authMiddleware, getNote);
router.delete('/:id', authMiddleware, deleteNote);
router.patch('/:id', authMiddleware, markMyNoteCompletedById);
router.put('/:id', authMiddleware, updateNoteById);

// router.post('/register', registerUser);

// router.post('/login', loginUser);

// router.get('/me', authMiddleware, getUserInfo);

module.exports = {
  notesRouter: router,
};
