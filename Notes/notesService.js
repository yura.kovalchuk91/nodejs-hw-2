const { Note } = require('../models/Notes');

function createNote(req, res) {
  const { text, completed } = req.body;
  const note = new Note({
    text,
    userId: req.user.userId,
    completed,
  });
  note.save()
    .then((saved) => {
      res.status(200).json({
        message: 'success',
        saved,
      });
    })
    .catch((err) => {
      res.status(400).send({ message: `Error: ${err}` });
    });
}

async function getNotes(req, res) {
  Note.find({ userId: req.user.userId }, '-__v')
    .skip(req.query.offset)
    .limit(req.query.limit)
    .then((result) => {
      console.log(`limit: ${req.query.limit}`);
      res.status(200).json({
        offset: req.query.offset,
        limit: req.query.limit,
        count: result.length,
        notes: result,
      });
    })
    .catch((err) => {
      res.status(400).send({ message: `Error: ${err}` });
    });
}

const getNote = (req, res) => Note.findById(req.params.id)
  .then((note) => {
    res.status(200).json({
      message: 'success',
      note,
    });
  })
  .catch((err) => {
    res.status(400).send({ message: `Error: ${err}` });
  });

const deleteNote = (req, res) => Note.findByIdAndDelete(req.params.id)
  .then((note) => {
    res.status(200).json({
      message: 'success',
      note,
    });
  })
  .catch((err) => {
    res.status(400).send({ message: `Error: ${err}` });
  });

const updateNoteById = (req, res) => {
  const { text } = req.body;
  return Note.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { text } })
    .then(() => {
      res.status(200).json({ message: 'Note text was updated' });
    });
};

const markMyNoteCompletedById = async (req, res) => {
  try {
    const object = await Note.findById(req.params.id);
    await Note.findByIdAndUpdate(
      { _id: req.params.id, userId: req.user.userId },
      { $set: { completed: !object.completed } },
    );
    res.status(200).json({ message: 'Note was marked completed' });
  } catch (err) {
    res.status(400).send({ message: `Error: ${err}` });
  }
};

module.exports = {
  createNote,
  getNotes,
  getNote,
  deleteNote,
  markMyNoteCompletedById,
  updateNoteById,
};
