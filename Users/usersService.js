const bcryptjs = require('bcryptjs');

const { User } = require('../models/User');

const getUserInfo = async (req, res) => {
  const { user } = req;
  res.status(200).json({ user });
};

const deleteUser = (req, res) => User.findByIdAndDelete(req.user.userId)
  .then(() => {
    res.status(200).json({
      message: `User ${req.user.username} was deleted`,
    });
  })
  .catch((err) => {
    res.status(400).send({ message: `Error: ${err}` });
  });

const updateUsername = (req, res) => {
  const { username } = req.user;
  return User.findByIdAndUpdate(
    { _id: req.params.id, userId: req.user.userId },
    { $set: { username: req.body.username } },
  )
    .then(() => {
      res.status(200).json({ message: `${username}'s username was changed to ${req.body.username}` });
    });
};

const updateUserPassword = async (req, res) => {
  const { username } = req.user;
  const { oldPassword, newPassword } = req.body;
  const salt = await bcryptjs.genSalt(10);
  const user = await User.findOne({ username });
  const passwordCompare = await bcryptjs.compare(String(oldPassword), String(user.password));
  if (!passwordCompare) {
    res.status(400).json({ message: `Old password must be equal to current ${username}'s password` });
    return;
  }
  const newPasswordHash = await bcryptjs.hash(newPassword, salt);
  return User.findOneAndUpdate(
    { username },
    { $set: { password: newPasswordHash } },
  )
    .then(() => {
      res.status(200).json({ message: `${username}'s password was changed` });
    });
};

module.exports = {
  getUserInfo,
  deleteUser,
  updateUsername,
  updateUserPassword,
};
