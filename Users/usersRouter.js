const express = require('express');

const router = express.Router();
const {
  getUserInfo, deleteUser, updateUsername, updateUserPassword,
} = require('./usersService');
const { authMiddleware } = require('../middleware/authMiddleware');

router.get('/me', authMiddleware, getUserInfo);
router.delete('/me', authMiddleware, deleteUser);
router.patch('/me', authMiddleware, updateUserPassword);
router.patch('/me/username', authMiddleware, updateUsername);

module.exports = {
  usersRouter: router,
};
