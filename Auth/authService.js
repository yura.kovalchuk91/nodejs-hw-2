const jwt = require('jsonwebtoken');
const bcryptjs = require('bcryptjs');
require('dotenv').config();
const { User } = require('../models/User');

const registerUser = async (req, res, next) => {
  const { username, password } = req.body;

  const salt = await bcryptjs.genSalt(10);

  const user = new User({
    username,
    password: await bcryptjs.hash(password, salt),
  });

  user.save()
    .then((saved) => res.json({
      message: 'success',
      saved,
    }))
    .catch((err) => {
      next(err);
    });
};

const loginUser = async (req, res) => {
  const token = process.env.SECRET_KEY;
  const user = await User.findOne({ username: req.body.username });
  if (user && await bcryptjs.compare(String(req.body.password), String(user.password))) {
    const payload = { username: user.username, userId: user._id };
    const jwt_token = jwt.sign(payload, token);
    return res.status(200).json({
      message: 'success',
      jwt_token,
    });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

const getUserInfo = async (req, res) => {
  res.status(200).send(req.user);
};

module.exports = {
  registerUser,
  loginUser,
  getUserInfo,
};
